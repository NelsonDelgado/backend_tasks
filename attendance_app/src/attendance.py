from src.summary import Summary
class Attendance:
    '''Attendance is a Meeting ocurrence, it has a summary and a list of participants.'''

    def __init__(self, start_datetime, summary=None, participants=list()):
        self.start_datetime = start_datetime
        self.summary = summary
        self.participants = participants
    
    def __init__(self, start_datetime, summary:Summary, participants=list()):
        self.start_datetime = start_datetime
        self.summary = summary
        self.participants = participants
        

    def add_participants(self, participants):
        self.participants += participants

    def show_participants(self):
        for participant in self.participants:
            print(participant)

    def __str__(self) -> str:
        return f"Start Time: {self.start_datetime}\nSummary = {self.summary}\nParticipants = {len(self.participants)}"

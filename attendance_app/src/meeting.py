from src.summary import Summary
class Meeting:
    '''Encapsulates a Meeting definition.'''

    def __init__(self, id, title, attendance=list()):
        self.attendance = attendance
        self.id = id
        self.title = title


    def add_attendance(self, attendance):
        self.attendance += attendance
    
    def __str__(self) -> str:
        return f"Id: {self.id}, Title: {self.title}, Attendance = {len(self.attendance)}"


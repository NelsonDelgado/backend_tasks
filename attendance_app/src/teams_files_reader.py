## TODO: Add whatever you need new classes or functions
from os import listdir
from os.path import isfile, join
import csv
from csv import *
from src.report_section_helper import extract_section_rows_list
from src.summary_builder import normalize_summary
from src.participant_builder import normalize_participant_short

# discovery de "data" files
#def discover_files(folder='../data', find_pattern='.csv'):

def discover_files(folder='./data/', find_pattern='csv'):
    return [join(folder, f) for f in listdir(folder) if isfile(join(folder, f)) and f.endswith(find_pattern)]


def get_file_data(path):
    data = []
    with open(path, 'r', encoding='UTF-16') as file:
        #data = list(DictReader(file, delimiter='\t'))
        data = list(csv.reader(file, delimiter='\t'))
    return data 

def normalize_raw_data(data):
    data_dict = {}
    if 'Meeting Summary' in data[0]:
        data_head = extract_section_rows_list(data, 'Meeting Summary', 'Full Name')
        list_body = extract_section_rows_list(data, 'Meeting Id', '')

    list_activities = []
    if '1. Summary' in data[0]:
        data_head = extract_section_rows_list(data, '1. Summary', '2. Participants')
        list_body = extract_section_rows_list(data, '2. Participants', '3. In-Meeting activities')
        list_activities = extract_section_rows_list(data, '3. In-Meeting activities')
    
    list_head = list_body.pop(0)
    data_dict = get_normalize_summary(data_head)
    participants = get_normalize_participants(list_body, list_head)

    data_dict['Participants'] = participants
    data_dict['In-Meeting activities'] = participants

    if len(list_activities) != 0:
        list_head = list_activities.pop(0)
        data_dict['In-Meeting activities'] = get_normalize_participants(list_activities, list_head)
    return data_dict

    
def get_normalize_summary(data_head):
    summary = { row[0]:row[1] for row in data_head}
    return normalize_summary(summary)


def get_normalize_participants(list_body, list_head):
    participants = []
    for row in  list_body:
        participant = { list_head[i] : row[i] for i in range(len(list_head)) }
        participants.append(normalize_participant_short(participant))
    return participants


def load_data():
    data = []
    file_paths = discover_files() or []
    for fp in file_paths:
        raw_data = get_file_data(fp) # each element is a string (a line in the file)    
        data_dict = normalize_raw_data(raw_data)
        data.append(data_dict)
    return data
    # pass

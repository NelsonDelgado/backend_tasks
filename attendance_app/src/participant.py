from src.duration import *
class Participant:

    def __init__(self, full_name: str, role: str, id: str, email: str, join_time: str, leave_time: str, in_meeting_duration: Duration):
        self.full_name = full_name
        self.role = role
        self.id = id
        self.email = email
        self.join_time = join_time
        self.leave_time = leave_time
        self.in_meeting_duration = in_meeting_duration

        self.name = self._get_name()
        self.middle_name = self._get_middle_name()
        self.first_surname = self._get_first_surname()
        self.second_surname = self._get_second_surname()


    def _get_name(self):
        list_full_name = self.full_name.split()
        return list_full_name[0]

    def _get_middle_name(self):
        list_full_name = self.full_name.split()
        if len(list_full_name) > 3:
            return list_full_name[1]
        return None

    def _get_first_surname(self):
        list_full_name = self.full_name.split()
        if len(list_full_name) > 3:
            return list_full_name[2]
        
        if len(list_full_name) > 1:
            return list_full_name[1]
        return None

    def _get_second_surname(self):
        list_full_name = self.full_name.split()
        if len(list_full_name) > 4:
            return list_full_name[3]

        if len(list_full_name) > 2:
            return list_full_name[-1]
        return None

    def __str__(self) -> str:
        return f"Full name: {self.full_name}, Role: {self.role}, Id: {self.id}, Email: {self.email}, Join: {self.join_time}, Leave: {self.leave_time}"
from src.teams_files_reader import load_data
from src.attendance_builder import build_attendance_object
from src.meeting import *

class TeamsAttendanceDatabase:

    def __init__(self):
        ## TODO read files to get RAW data (dictionary, list and primitive datatypes)
        ## load_data function is the access point to read data files
        self.__attendance = load_data() or []

    # the client (caller) will call as object.get_meetings()
    def get_meetings(self):
        return [self.__create_meeting(**raw_attendance) for raw_attendance in self.__attendance]

    # @property -> the client (caller) will call as object.meetings (reference as a attribute)
    @property
    def meetings(self):
        ## raw_attendance represents content of data file as raw "dict"
        ## Review __create_meeting method definition for required key/value pairs 
        ## missing values for required keys must be set to None
        return [self.__create_meeting(raw_attendance) for raw_attendance in self.__attendance]
        #return [self.__create_meeting(**raw_attendance) for raw_attendance in self.__attendance]
        
    def __create_meeting(self, raw_data):
        id = raw_data.get('Id')
        title  = raw_data.get('Title')
        attendances = [build_attendance_object(raw_data)]
        return Meeting(id, title, attendances)
    
    #def __create_meeting(self, meeting_id, meeting_title, start_time, end_time, meeting_duration, attended_participants, participants, in_meeting_activities):
    #     id = id
    #     title  = title
    #     attendances = [] # List of elements of type Attendance
    #     return Meeting(id, title, attendances)
from src.duration_helper import *
from src.participant import Participant
from src.duration import Duration

def normalize_participant_short(raw: dict):
    result = {}
    for key, element in raw.items():
        if 'name' in key.lower():
            result['Full Name'] = element
        if 'join' in key.lower():
            result['Join time'] = get_date_format_yy(element)
        if 'leave' in key.lower():
            result['Leave time'] = get_date_format_yy(element)
        if 'email' in key.lower():
            result['Email'] = element
            result['Participant ID (UPN)'] = element
        if 'role' in key.lower():
            result['Role'] = element
        if 'participant' in key.lower():
            result['Participant ID (UPN)'] = element
        if 'duration' in key.lower():
            duration = read_str_duration(element)
            result['Duration'] = {
                'Hours': duration.get('hours'),
                'Minutes': duration.get('minutes'),
                'Seconds': duration.get('seconds')
            }
    return result


def normalize_participant(raw: dict):
    result = {}
    for key, element in raw.items():
        if 'name' in key.lower():
            result['Full Name'] = element
        if 'join time' in key.lower():
            result['Join time'] = get_date_format_yy(element)
        if 'leave time' in key.lower():
            result['Leave time'] = get_date_format_yy(element)
        if 'email' in key.lower():
            result['Email'] = element
            result['Participant ID (UPN)'] = element
        if 'role' in key.lower():
            result['Role'] = element
        if 'participant' in key.lower():
            result['Participant ID (UPN)'] = element
        if 'duration' in key.lower():
            duration = read_str_duration(element)
            result['In-meeting Duration'] = {
                'Hours': duration.get('hours'),
                'Minutes': duration.get('minutes'),
                'Seconds': duration.get('seconds')
            }
    return result


def resolve_duration(name, raw):
    duration = []
    for row in raw:
        if name == row.get('Full Name'):
            duration.append(row['Duration'])
    return sum_list_durations(duration)


#Este funcion, no clasifica la fecha mas antigua a la mas reciente.
#Solo retorna en primer 'Join Time' y el utimo 'Leave Time de la lista'
def get_participants(name, raw):
    result = {}
    ctn = 0
    for row in raw:
        if name == row.get('Full Name') and ctn == 0:
            ctn += 1
            result['Name'] = row.get('Full Name')
            result['First Join Time'] = row.get('Join Time')
            result['Last Leave time'] = row.get('Leave Time')
            result['Email'] = row.get('Email')
            result['Role'] = row.get('Role')
            result['Participant ID (UPN)'] = row.get('Participant ID (UPN)')

        if name == row.get('Full Name') and ctn > 0:
            result['Last Leave time'] = row.get('Leave Time')
    return result   


def resolve_participant_join_last_time(raw: dict):
    participants = []
    list_names = list(set([row.get('Full Name') for row in raw]))
    for name in list_names:
        participant = get_participants(name, raw)
        duration = resolve_duration(name, raw)
        participant['In-meeting Duration'] = f"{duration['hours']}h {duration['minutes']}m {duration['seconds']}s"
        participants.append(participant) 
    return participants[0]


def build_participant_object(raw: dict):      
    if raw.get('Full Name') == None or raw.get('Join time') == None or raw.get('Leave time') == None:
        raise ValueError('Full Name, Join time, Leave time cannot be None')

    return Participant (
        raw.get('Full Name'),
        raw.get('Role'),
        raw.get('Participant ID (UPN)'),
        raw.get('Email'),
        raw.get('Join time'),
        raw.get('Leave time'),
        Duration(
            raw['In-meeting Duration'].get('Hours'),
            raw['In-meeting Duration'].get('Minutes'),
            raw['In-meeting Duration'].get('Seconds')
            )
        )

from datetime import datetime, timedelta

DATE_MMDDYY = "%m/%d/%y, %I:%M:%S %p"
DATE_MMDDYYYY = "%m/%d/%Y, %I:%M:%S %p"

def get_elapsed_duration(start_date, end_date):
    if start_date == None or start_date == '' or end_date == None or end_date == '':
        return None
    
    start_dt = datetime.strptime(start_date,DATE_MMDDYY)
    end_dt = datetime.strptime(end_date, DATE_MMDDYY)
    elapsed = end_dt - start_dt 

    if elapsed.days < 0:
        return None
    return elapsed


def get_dict_duration(data):
    if data == None or data == '':
        return None
    s = data.seconds
    m = int(s/60)
    h = int(m/60)
    return {
        'hours': h + (data.days * 24),
        'minutes': m % 60,
        'seconds': s - (h * 3600) - (m%60 * 60)
    }


def get_date_format_yy(date_text):
    result = ""
    if len(date_text) >= 22:
        result = str(datetime.strptime(date_text, DATE_MMDDYYYY).strftime(DATE_MMDDYY)).split()
    if len(date_text) < 22:
        result = str(datetime.strptime(date_text, DATE_MMDDYY).strftime(DATE_MMDDYY)).split()
    return f"{result[0].lstrip('0')} {result[1].lstrip('0')} {result[2].lstrip('0')}"


def get_duration(start_date, end_date):
    start_dt = get_date_format_yy(start_date)
    end_dt = get_date_format_yy(end_date)

    elapsed = get_elapsed_duration(start_dt, end_dt)
    return get_dict_duration(elapsed)


def read_str_duration(str_duration):
    duration = {
        'hours': 0,
        'minutes': 0,
        'seconds': 0 }
    for e in str_duration.split():
        if 'h' in e.lower():
            duration['hours'] = int(e.lower().replace('h', ''))
        if 'm' in e.lower():
            duration['minutes'] = int(e.lower().replace('m', '')) 
        if 's' in e.lower():
            duration['seconds'] = int(e.lower().replace('s', ''))
    return duration

def sum_list_durations(list_dir):
    duration = {
        'hours': 0,
        'minutes': 0,
        'seconds': 0 }
    for row in list_dir:
        dir_2 = read_str_duration(row)
        duration = sum_dict_durations(duration, dir_2)
    return duration


def sum_dict_durations(dir_1, dir_2):
    s = dir_1['seconds'] + dir_2['seconds']
    m = dir_1['minutes'] + dir_2['minutes']
    h = dir_1['hours'] + dir_2['hours']
    if s >= 60:
        s = s - 60
        m = m + 1
    if m >= 60:      
        m = m - 60
        h = h + 1
    return {
        'hours': h,
        'minutes': m,
        'seconds': s
    }

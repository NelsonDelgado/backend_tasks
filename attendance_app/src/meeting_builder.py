# from src.meeting import Meeting
# from src.summary import Summary
# from src.participant import Participant
# from src.duration import Duration

# def build_meeting_object(raw):
#     summary = Summary(
#         raw.get('Title'),
#         raw.get('Id'),
#         raw.get('Attended participants'),
#         raw.get('Start Time'),
#         raw.get('End Time'),
#         Duration(
#             raw.get('Duration').get('hours'),
#             raw.get('Duration').get('minutes'),
#             raw.get('Duration').get('seconds')
#             )
#         )
#     participants = []
#     for row in raw.get('Participants'):
#         participants.append(
#             Participant (
#                 row.get('Full Name'),
#                 row.get('Role'),
#                 row.get('Participant ID (UPN)'),
#                 row.get('Email'),
#                 row.get('Join time'),
#                 row.get('Leave time'),
#                 Duration(
#                     row['Duration'].get('Hours'),
#                     row['Duration'].get('Minutes'),
#                     row['Duration'].get('Seconds')
#                 )
#             )
#         )
#     return Meeting(raw.get('Id'), raw.get('Title'), summary, participants)
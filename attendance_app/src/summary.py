from src.duration import Duration
#from duration import Duration

class Summary:
    def __init__(self, title, id, attended_participants, start_time, end_time, duration: Duration):
        self.title = title
        self.id = id
        self.attended_participants = attended_participants
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration

    def __str__(self) -> str:
        return f"Title: {self.title}, Id: {self.id}, Participants: {self.attended_participants}, Start: {self.start_time}, End: {self.end_time}, Duration: {self.duration}"
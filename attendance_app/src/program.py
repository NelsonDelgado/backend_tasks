from teams_attendance_database import TeamsAttendanceDatabase
from meeting import Meeting

def main():
    meeting_database = TeamsAttendanceDatabase()
    meetings = meeting_database.meetings
    assert meetings is not None
    assert type(meetings) is list
    assert all(isinstance(m, Meeting) for m in meetings)

if __name__ == "__main__":
    main()
from src.duration_helper import *
from src.summary import Summary
from src.duration import Duration

def normalize_summary(raw_data: dict):
    summary = {}
    for key, row in raw_data.items():
        if 'title' in key.lower():
            summary['Title'] = row
            summary['Id'] = row
        if 'id' in key.lower():
            summary['Id'] = row
        if 'participants' in key.lower():
            summary['Attended participants'] = int(row)
        if 'start time' in key.lower():
            summary['Start Time'] = row
        if 'end time' in key.lower():
            summary['End Time'] = row
    summary['Duration'] = get_duration(summary['Start Time'], summary['End Time'])
    return summary


def build_summary_object(raw_data: dict):
    return Summary(
        raw_data.get('Title'),
        raw_data.get('Id'),
        raw_data.get('Attended participants'),
        raw_data.get('Start Time'),
        raw_data.get('End Time'),
        Duration(
            raw_data['Duration'].get('hours'),
            raw_data['Duration'].get('minutes'),
            raw_data['Duration'].get('seconds')
            )
        )

from src.participant import *
from src.participant_builder import *
from src.duration_helper import *
from src.summary_builder import *
from src.teams_attendance_database import *
from src.meeting import *


def main():
    meeting_database = TeamsAttendanceDatabase()
    meetings = meeting_database.meetings
    assert meetings is not None
    assert type(meetings) is list
    assert all(isinstance(m, Meeting) for m in meetings)

if __name__ == "__main__":
    main()


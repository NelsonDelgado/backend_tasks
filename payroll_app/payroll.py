class PayrollSystem:

    def calculate_payroll(self, employees=None):
        if employees == None:
            return
        result = [(employee, employee.calculate_payroll()) for employee in employees]
        [ print( f"Payroll for: {e}\ncheck amount: {p}") for e, p in result ]
        return result


        # for employee in employees:
        #     print(f'Payroll for: {employee}')
        #     print(f'Check amount: {employee.calculate_payroll()}')
import pytest
from employee import Employee, SalesAssociate, AdmistrativeWorker, ManufacturingWorker


def test_employee_expects_typeerror():
    match='Can\'t instantiate abstract class Employee with abstract method calculate_payroll'
    with pytest.raises(expected_exception=TypeError, match=match):
        Employee(100, 'name')


def test_AdmistrativeWorker():
    employee = AdmistrativeWorker(1, 'John Doe', 1500)
    
    assert employee is not None, 'expected not None'
    assert type(employee) is AdmistrativeWorker, 'expected type AdmistrativeWorker'
    assert employee.id == 1, 'expected employee.id == 1'
    assert employee.name == 'John Doe', 'expected employee.name == John Doe'


def test_calculate_payroll_AdmistrativeWorker():
    employee = AdmistrativeWorker(1, 'John Doe', 1500)

    assert employee.calculate_payroll() == 1500, 'expected calculate_payroll == 1500'


def test_ManufacturingWorker():
    employee = ManufacturingWorker(2, 'Jane Doe', 40, 15)
    
    assert employee is not None, 'expected not None'
    assert type(employee) is ManufacturingWorker, 'expected type ManufacturingWorker'
    assert employee.id == 2, 'expected employee.id == 2'
    assert employee.name == 'Jane Doe', 'expected  employee.name == Jane Doe'


def test_calculate_payroll_ManufacturingWorker():
    employee = ManufacturingWorker(2, 'Jane Doe', 40, 15)

    assert employee.calculate_payroll() == 600, 'expected calculate_payroll == 600'


def test_SalesAssociate():
    employee = SalesAssociate(3, 'Foo Fighter', 1000, 250)
    
    assert employee is not None, 'expected not None'
    assert type(employee) is SalesAssociate, 'expected type SalesAssociate'
    assert employee.id == 3, 'expected employee.id == 3'
    assert employee.name == 'Foo Fighter', 'expected employee.name == Foo Fighter'


def test_calculate_payroll_SalesAssociate():
    employee = SalesAssociate(3, 'Foo Fighter', 1000, 250)

    assert employee.calculate_payroll() == 1250, 'expected calculate_payroll == 1250'
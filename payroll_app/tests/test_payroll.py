from employee import SalesAssociate, AdmistrativeWorker, ManufacturingWorker
import payroll as hr

def test_calculate_payroll_none_list():
    payroll_system = hr.PayrollSystem()
    result = payroll_system.calculate_payroll()
    assert result is None, 'expected None'


def test_calculate_payroll_empty_list():
    payroll_system = hr.PayrollSystem()
    assert len(payroll_system.calculate_payroll([])) == 0, 'expected None'


def test_calculate_payroll_non_empty_list():
    payroll_system = hr.PayrollSystem()
    result = payroll_system.calculate_payroll([
        AdmistrativeWorker(1, 'John Doe', 1500),
        ManufacturingWorker(2, 'Jane Doe', 40, 15),
        SalesAssociate(3, 'Foo Fighter', 1000, 250)
    ])

    assert len(result) != 0, 'expected non None'